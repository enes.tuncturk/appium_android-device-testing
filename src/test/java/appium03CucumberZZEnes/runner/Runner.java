package appium03CucumberZZEnes.runner;


import io.cucumber.testng.AbstractTestNGCucumberTests;
import io.cucumber.testng.CucumberOptions;

@CucumberOptions(
        features={"src/test/java/appium03CucumberZZEnes/features/Apidemos1.feature"}
        , glue = {"appium03CucumberZZEnes/stepdefs"}
)
public class Runner extends AbstractTestNGCucumberTests {

}
