package appium03CucumberZZEnes.stepdefs;

import appium03CucumberZZEnes.base.BaseStep;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import utils.App;
import utils.Device;

public class MyStepdefs extends BaseStep {
    @Given("^user opens (APIDEMO|CALCULATOR) on (PIXEL_3XL)$")
    public void userOpensOnThe(String app, String device) {
        openApp(Device.valueOf(device), App.valueOf(app));
        click("CONTINUE");
        click("OK");
        click("OK");
    }

    @When("user clicks {string}")
    public void userClicks(String text) {
        click(text);
    }

    @Then("{string} should be visible")
    public void shouldBeVisible(String text) {
        shouldBeVisible(text);
    }

    @And("swipe until the text {string} is visible")
    public void swipeUntilTheTextIsVisible(String text) {
        swipeUntilVisible(text,true,0.3);
    }
}
