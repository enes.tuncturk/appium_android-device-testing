package example;

import org.openqa.selenium.By;

public class Locators {

    public static final By lContinue = By.id("com.android.permissioncontroller:id/continue_button");
    public static final By lOk = By.id("android:id/button1");
    public static final By lOk2 = By.id("com.touchboarder.android.api.demos:id/buttonDefaultPositive");
    public static final By lCountry = By.id("com.touchboarder.android.api.demos:id/edit");

    public static final By lHelloWord = By.xpath("//android.view.View[@content-desc=\"Hello World! - 1\"]/android.widget.TextView");
    public static final By stopWatch = By.xpath("//androidx.appcompat.app.ActionBar.Tab[@content-desc=\"Stopwatch\"]/android.widget.TextView");

    public static final By lStart = By.xpath("//android.widget.ImageButton[@content-desc=\"Start\"]");
    public static final By lSecond = By.id("com.android.deskclock:id/stopwatch_time_text");



}
