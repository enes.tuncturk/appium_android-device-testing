package example;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import static example.Locators.*;

public class Test1_ApiDemos extends Base {




    @Test
    public void test1() throws InterruptedException {

        click(lContinue);
        click(lOk);
        click(lOk2);
        click("API Demos");
        click("Views");
        click("Auto Complete");
        click("1. Screen Top");
        sendKeys(lCountry,"turkey");
        driver.hideKeyboard();
        sleep(3);
        driver.openNotifications();
        sleep(3);
        driver.navigate().back();
        driver.getBatteryInfo();
        sleep(3);


    }
    @Test
    public void test2() throws InterruptedException {

        click(lContinue);
        click(lOk);
        click(lOk2);
        click("API Demos");
        System.out.println(driver.getOrientation());
        driver.rotate(ScreenOrientation.LANDSCAPE);
        waitForOrientation(ScreenOrientation.LANDSCAPE);
        sleep(4);
        System.out.println(driver.getOrientation());
        swipeUntilVisible(getLocatorWithText("Views"),true,0.4);
        click("Views");
        swipeUntilVisible(getLocatorWithText("WebView"),true,0.8);
        click("WebView");
        waitForVisibility(getLocatorWithText("Hello World! - 1"));
        driver.rotate(ScreenOrientation.PORTRAIT);
        waitForOrientation(ScreenOrientation.PORTRAIT);
        sleep(4);




    }
    @Test
    public void test3() throws InterruptedException {

        click("STOPWATCH");
        sleep(5);
        click(lStart);
        //wait.until(driver.findElement(lSecond).getText()=="5");
        wait.until(ExpectedConditions.textToBePresentInElementLocated(lSecond,"5"));
        sleep(5);






    }


}
