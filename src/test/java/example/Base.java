package example;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.By;
import org.openqa.selenium.ScreenOrientation;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;

public class Base {

    AndroidDriver<MobileElement> driver;
    DesiredCapabilities caps = new DesiredCapabilities();
    WebDriverWait wait;


    @BeforeTest
    public void beforeTest() throws MalformedURLException {

        caps.setCapability("appium:udid", "emulator-5554");
        caps.setCapability("appium:version", "12");
        caps.setCapability("appium:deviceName", "EnesVirtualPhone");
        caps.setCapability("platformName", "Android");
        //caps.setCapability("appium:appPackage", "com.touchboarder.android.api.demos");
        //caps.setCapability("appium:appActivity", "com.touchboarder.androidapidemos.MainActivity");
        caps.setCapability("appium:appPackage", "com.android.deskclock");
        caps.setCapability("appium:appActivity", "com.android.deskclock.DeskClock");
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), caps);
        wait = new WebDriverWait(driver,20);
    }

    @AfterTest
    public void afterTest() {

        driver.quit();
        driver = null;

    }

    public By getLocatorWithText(String text){

        return By.xpath("//*[@text='"+ text +"']");

    }

    public void click(String text){

        By locator = getLocatorWithText(text);
        click(locator);

    }
    public void click(By locator){
        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }



    public void sendKeys(By locator, String text){

        wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).sendKeys(text);

    }
    public void sleep(int seconds) throws InterruptedException {
        Thread.sleep(seconds*1000);
    }

    public void swipeV(double start, double end) {
        int width = driver.manage().window().getSize().getWidth();
        int height = driver.manage().window().getSize().getHeight();

        if (start < .1) start = .1;
        if (start > .9) start = .9;

        if (end < .1) end = .1;
        if (end > .9) end = .9;

        int startPoint = (int) (height * start);
        int endPoint = (int) (height * end);

        new TouchAction<>(driver)
                .press(PointOption.point(width / 2, startPoint))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point(width / 2, endPoint))
                .release()
                .perform();
    }

    public void swipeH(double start, double end) {
        int width = driver.manage().window().getSize().getWidth();
        int height = driver.manage().window().getSize().getHeight();

        if (start < .1) start = .1;
        if (start > .9) start = .9;

        if (end < .1) end = .1;
        if (end > .9) end = .9;

        int startPoint = (int) (width * start);
        int endPoint = (int) (width * end);

        new TouchAction<>(driver)
                .press(PointOption.point(startPoint, height / 2))
                .waitAction(WaitOptions.waitOptions(Duration.ofMillis(300)))
                .moveTo(PointOption.point(endPoint, height / 2))
                .release()
                .perform();
    }



    public void swipeUntilVisible(By locator, boolean down, double speed) {
        if (speed < 0.2) speed = .2;
        if (speed > 0.8) speed = .8;

        speed /= 2;

        while (true) {
            try {
                if (driver.findElement(locator).isDisplayed())
                    break;
            } catch (Exception e) {
                if (down)
                    swipeV(.5+speed, .5-speed);
                else
                    swipeV(.5-speed, .5+speed);
            }
        }
    }

    public void swipeUntilVisible1(By locator, boolean down) {

        while (true) {
            if (driver.findElements(locator).size() > 0)
                break;

            if (down)
                swipeV(.6, .5);
            else
                swipeV(.4, .5);
        }
    }
    public void waitForVisibility(By locator){

        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));

    }

    public void waitForOrientation(ScreenOrientation screenOrientation){

        wait.until(driver->{
          try{
              if(((AndroidDriver)driver).getOrientation()==screenOrientation)
                  return true;
              return false;
          } catch (Exception e){
              return false;
          }

        });
    }

}
