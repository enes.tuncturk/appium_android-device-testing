package appium01ZZEnes;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;

public class Test1_Connection {

    @Test
    public void testConnection01() throws MalformedURLException {

        /*
        1-Appium server calismali
        2-Appium Driver
            AndroidDriver
            IOSDriver
        2-Cihaz ve emülator baglantisi
        3-Hangi uygulama calistirilacak

         */

        // 1. Appium 4723 portundan calistirilir. GUI ya da console'dan run edilebilir.
        // 2. AppiumDriver definition
        AppiumDriver<MobileElement> driver;

        // 3 ve 4 Cihaz ve app bilgisi, DesiredCapabilities

        DesiredCapabilities caps = new DesiredCapabilities();

        // Baglanilacak cihaz
        caps.setCapability("appium:udid", "emulator-5554");
        caps.setCapability("appium:version", "12");
        caps.setCapability("appium:deviceName", "EnesVirtualPhone");
        caps.setCapability("platformName", "Android");

        // App Bilgisi appPackage appActivity
        // kullanilacak app acilir ve consolda
        // adb shell
        // dumpsys window | grep -E 'mCurrentFocus'
        // Output: com.google.android.apps.nexuslauncher/com.google.android.apps.nexuslauncher.NexusLauncherActivity
        // com.touchboarder.android.api.demos/com.touchboarder.androidapidemos.MainActivity

        // mCurrentFocus=Window{91f4547 u0 com.android.permissioncontroller/com.android.permissioncontroller.permission.ui.ReviewPermissionsActivity
        caps.setCapability("appium:appPackage","com.touchboarder.android.api.demos");
        caps.setCapability("appium:appActivity","com.touchboarder.androidapidemos.MainActivity");

        // driver definition

        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"),caps);

        //By chrome = By.xpath("//android.widget.TextView[@content-desc=\"Chrome\"]");
        //driver.findElement(chrome).click();

        By lapiDemo = By.xpath("//android.widget.TextView[@content-desc='API Demos']");

        By lcontinue = By.id("com.android.permissioncontroller:id/continue_button");

        driver.findElement(lapiDemo).click();
        driver.findElement(lcontinue).click();




    }

}
