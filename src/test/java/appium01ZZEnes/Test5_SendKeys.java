package appium01ZZEnes;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class Test5_SendKeys {
    AppiumDriver<MobileElement> driver;
    WebDriverWait wait;
    By lapiDemo = By.xpath("//android.widget.TextView[@content-desc='API Demos']");
    By lcontinue = By.id("com.android.permissioncontroller:id/continue_button");

    //appium'u programsal olarak calistirmak icin
    AppiumDriverLocalService service;


    @BeforeTest
    public void beforeTest() throws MalformedURLException {

        startAppium();
        driver = new AndroidDriver<>(service.getUrl(), caps());
        wait = new WebDriverWait(driver,20);


    }

    @AfterTest
    public void afterTest() {
        driver.closeApp();
        driver.quit();
        service.stop();

    }


    @Test
    public void test1() {


        //driver.findElement(lapiDemo).click();
        //wait.until(ExpectedConditions.elementToBeClickable(lapiDemo)).click();
        click(lapiDemo);

        //driver.findElement(lcontinue).click();
        //wait.until(ExpectedConditions.elementToBeClickable(lcontinue)).click();
        click(lcontinue);

        /*click(getXPathOfText("API Demos"));
        click(getXPathOfText("Views"));
        click(getXPathOfText("Controls"));
        click(getXPathOfText("01.Light Thema"));
         */
        click("API Demos");
        click("Views");
        click("Controls");
        click("01.Light Thema");
        sendKeys(lapiDemo, "Guidersoft" ); // -> locator degismesi gerek, örnek olarak yazdim
        click(getXPathWithAttr("check1"));
        click(getXPathWithAttr("check2"));




    }

    public DesiredCapabilities caps() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("appium:udid", "emulator-5554");
        caps.setCapability("appium:version", "12");
        caps.setCapability("appium:deviceName", "EnesVirtualPhone");
        caps.setCapability("platformName", "Android");
        caps.setCapability("appium:appPackage", "com.android.permissioncontroller");
        caps.setCapability("appium:appActivity", "com.android.permissioncontroller.permission.ui.ReviewPermissionsActivity");
        return caps;
    }

    public void startAppium(){
        service = new AppiumServiceBuilder()
                .withIPAddress("127.0.0.1") // ip adresinden
                //.usingPort(4723) // port numarasi
                .usingAnyFreePort() // o an hangi port bossa ondan run edilecek
                .build();

        //consolda log kayitlarini yazmaz
        service.clearOutPutStreams();
        service.start();

    }

    public void click(String text){
        By locator = getXPathOfText(text);
        click(locator);
    }
    public void click(By locator){
        wait.until(ExpectedConditions.elementToBeClickable(locator)).click();
    }
    public void sendKeys(By locator, String text){
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator)).sendKeys(text);
    }
    public By getXPathOfText(String text){
        return By.xpath("//*[@text='" +text + "']");
    }
    public By getXPathWithAttr(String text){
        return By.xpath("//*[@*[contains(., '" +text + "')]]");
    }

}
