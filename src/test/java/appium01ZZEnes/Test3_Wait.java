package appium01ZZEnes;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.service.local.AppiumDriverLocalService;
import io.appium.java_client.service.local.AppiumServiceBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.net.MalformedURLException;

public class Test3_Wait {
    AppiumDriver<MobileElement> driver;
    WebDriverWait wait;
    By lapiDemo = By.xpath("//android.widget.TextView[@content-desc='API Demos']");
    By lcontinue = By.id("com.android.permissioncontroller:id/continue_button");

    //appium'u programsal olarak calistirmak icin
    AppiumDriverLocalService service;


    @BeforeTest
    public void beforeTest() throws MalformedURLException {

        startAppium();
        driver = new AndroidDriver<>(service.getUrl(), caps());
        wait = new WebDriverWait(driver,20);


    }

    @AfterTest
    public void afterTest() {
        driver.closeApp();
        driver.quit();
        service.stop();

    }


    @Test
    public void test1() {


        //driver.findElement(lapiDemo).click();
        wait.until(ExpectedConditions.elementToBeClickable(lapiDemo)).click();
        //driver.findElement(lcontinue).click();
        wait.until(ExpectedConditions.elementToBeClickable(lcontinue)).click();


    }

    public DesiredCapabilities caps() {
        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("appium:udid", "emulator-5554");
        caps.setCapability("appium:version", "12");
        caps.setCapability("appium:deviceName", "EnesVirtualPhone");
        caps.setCapability("platformName", "Android");
        caps.setCapability("appium:appPackage", "com.android.permissioncontroller");
        caps.setCapability("appium:appActivity", "com.android.permissioncontroller.permission.ui.ReviewPermissionsActivity");
        return caps;
    }

    public void startAppium(){
        service = new AppiumServiceBuilder()
                .withIPAddress("127.0.0.1") // ip adresinden
                //.usingPort(4723) // port numarasi
                .usingAnyFreePort() // o an hangi port bossa ondan run edilecek
                .build();

        //consolda log kayitlarini yazmaz
        service.clearOutPutStreams();
        service.start();

    }
}
