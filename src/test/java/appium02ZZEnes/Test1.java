package appium02ZZEnes;
import base.BaseTest;
import org.testng.annotations.Test;


public class Test1 extends BaseTest {



    @Test
    public void test1(){

    click("CONTINUE");
    click("OK");
    click("OK");


    }
    @Test(dependsOnMethods = "test1")
    public void test2(){
        click("API Demos");
        click("Views");
        swipeV(.7,.4);
        swipeV(.4,.7);
        swipeV(.7,.4);
        click("TextSwitcher");

        for (int i = 0; i < 5; i++) {
            click("NEXT");
            waitForVisibilityOf(String.valueOf(i+1));
        }

    }

    @Test(dependsOnMethods = "test1")
    public void test3_SwipeUntilVisible(){
        click("API Demos");
        click("Views");
        swipeUntilVisible("WebView",true,0.3);
        swipeUntilVisible("Animation",false,0.3);

    }


}
