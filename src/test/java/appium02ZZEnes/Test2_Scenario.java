package appium02ZZEnes;

import base.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

public class Test2_Scenario extends BaseTest {

    /*
    Scenario:
    a. Click Support Design Demos
    b. Click Text Input
    c. Send "myUser" to username
    d. Assert that the username test is "myUser"
    e. Click "SHOW ERROR" button
    f. Assert that error notification is visible
    g. Click "CLEAR ERROR"
    h. Assert that error notification is not visible
     */
    By lNotification = By.xpath("//TextInputLayout[.//*[@*[contains(.,'edit_username')]]]//android.widget.TextView");

    @Test
    public void testScenario() {

        click("CONTINUE");
        click("OK");
        click("OK");


    }

    @Test(dependsOnMethods = "testScenario")
    public void test2() {

        click("Support Design Demos");
        click("Text Input");
        sendKeys(getXPathOfTextAttr("edit_username"), "myUser");
        waitForVisibilityOf("myUser");
        click("SHOW ERROR");
        Assert.assertTrue(driver.findElement(lNotification).isDisplayed());
        click("CLEAR ERROR");
        waitForInvisibilityOf(lNotification);





    }

}
